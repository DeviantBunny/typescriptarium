"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const user_1 = require("./user");
const auth_1 = require("./auth");
const auth_2 = require("./auth");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
dotenv_1.default.config();
const app = (0, express_1.default)();
const port = process.env.PORT;
app.set('view engine', 'pug');
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.use((0, cors_1.default)());
app.get('/', (req, res) => {
    res.render('index');
});
app.get('/search', (req, res) => {
    var _a;
    //TODO: move token auth to Auth class
    let key = process.env.KEY;
    let token = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split("Bearer ")[1];
    // console.log("key", key);
    try {
        let decodedToken = jsonwebtoken_1.default.verify(token, key, {});
        //Further crunching
    }
    catch (exception) {
        res.status(666);
        res.send({ message: "Invalid token", exception: exception });
    }
    // res.render('search');
});
app.get('/register', (req, res) => {
    res.render('register');
});
app.post('/createUser', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let data = req.body;
    let user = new user_1.User(data.username, data.password);
    if (yield auth_2.Register.CreateUser(user)) {
        res.send('Success');
    }
    else {
        res.send('Failure');
    }
}));
app.post('/login', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let data = req.body;
    console.log("headers", req.headers);
    let user = new user_1.User(data.username, data.password);
    if (yield auth_1.Auth.CheckCreds(user)) {
        //res.send('Success') 
        let token = auth_1.Auth.GetToken(user);
        res.json({
            token
        });
    }
    else {
        res.send('Failure');
    }
}));
app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});
