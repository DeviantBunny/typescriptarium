"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
class User {
    constructor(username, password) {
        if (!username || !password) {
            throw new Error("Username and password can't be empty");
        }
        this.username = username;
        this.password = password;
    }
}
exports.User = User;
