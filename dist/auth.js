"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Register = exports.Auth = void 0;
const user_1 = require("./user");
const fs_1 = __importDefault(require("fs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
let users;
fs_1.default.readFile('users.json', 'utf8', (err, data) => {
    if (err) {
        console.log(err);
    }
    else {
        users = JSON.parse(data);
    }
});
class Auth {
    static CheckCreds(params) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = users.find(x => x.username === params.username);
            if (!user)
                return false;
            let isValidPass = bcryptjs_1.default.compareSync(params.password, user.password);
            if (isValidPass) {
                return true;
            }
            else {
                return false;
            }
        });
    }
    static GetToken(params) {
        let key = process.env.KEY;
        let token = jsonwebtoken_1.default.sign({ username: params.username }, key, { expiresIn: 5 });
        return token;
    }
    static storeTokenFE(token) {
        //Sets the token
        localStorage.setItem("token", token);
        //Get the token
        localStorage.getItem("token");
        localStorage.removeItem("token");
    }
}
exports.Auth = Auth;
class Register {
    static CreateUser(params) {
        return __awaiter(this, void 0, void 0, function* () {
            //Check if user exists
            let user = users.find(x => x.username === params.username);
            if (user)
                return false;
            else {
                //Instantiate a newUser and hash the password
                let pass = yield bcryptjs_1.default.hash(params.password, 8);
                let newUser = new user_1.User(params.username, pass);
                //Append new user to users and serialize to JSON
                users.push(newUser);
                let data = JSON.stringify(users);
                fs_1.default.writeFile('users.json', data, (err) => {
                    if (err) {
                        console.log(err);
                    }
                });
                return true;
            }
        });
    }
}
exports.Register = Register;
