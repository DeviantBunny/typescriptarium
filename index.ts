import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';

import cors from 'cors';

import { User } from './user';
import { Auth } from './auth';
import { Register } from './auth';

import jwt, {Secret} from "jsonwebtoken";


dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.set('view engine', 'pug');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}))
app.use(cors());

app.get('/', (req: Request, res: Response) => {
  res.render('index');
});

app.get('/search', (req: Request, res: Response) => {

   //TODO: move token auth to Auth class
  let key: Secret = process.env.KEY as string;

  let token: string = req.headers.authorization?.split("Bearer ")[1] as string;
  // console.log("key", key);
  try{
    let decodedToken = jwt.verify(token, key, {});

    //Further crunching
  }
  catch (exception){
    res.status(666);
    res.send({message:"Invalid token", exception: exception});
  }

  // res.render('search');
});

app.get('/register', (req: Request, res: Response) => {
  res.render('register');
});

app.post('/createUser', async (req: Request, res: Response) => {

  let data = req.body;

  let user = new User(data.username, data.password)
  if(await Register.CreateUser(user)) {res.send('Success') }
    else {res.send('Failure');}
});

app.post('/login', async (req: Request, res: Response) => {

  let data = req.body;

  console.log("headers", req.headers);

  let user = new User(data.username, data.password)
  if(await Auth.CheckCreds(user)) {
    //res.send('Success') 
    let token = Auth.GetToken(user);
    res.json({
      token
    })
    
  }
    else {res.send('Failure');}
});

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});

