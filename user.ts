export class User{
    username: string;
    password: string;

    constructor(username: string, password: string) {
        if (!username || !password) {
            throw new Error("Username and password can't be empty")
        }
        this.username = username;
        this.password = password;
    }
}

