import express, { Express } from "express";

const app: Express = express();

app.set('view engine', 'pug');