import { User } from "./user";
import fs from "fs";
import jwt, { Secret } from "jsonwebtoken";
import bcrypt from "bcryptjs";



let users : User[];
 fs.readFile('users.json', 'utf8', (err, data) => {
  if (err) {
    console.log(err);
  }
  else{
    users = JSON.parse(data);
  }
})

export class Auth {
  
    static async CheckCreds(params:User) {

        let user = users.find(x => x.username === params.username);
        if (!user) return false;

        let isValidPass = bcrypt.compareSync(
          params.password,
          user.password
        )

        if (isValidPass){
          return true;
        } 
        
        else {
          return false;}
    }

    static GetToken(params: User){
      let key: Secret = process.env.KEY as string;
      let token = jwt.sign({username: params.username}, key, {expiresIn: 5})
      return token;
    }

    static storeTokenFE (token: string) {
      //Sets the token
      localStorage.setItem("token", token);

      //Get the token
      localStorage.getItem("token");

      localStorage.removeItem("token");
    }
}

export class Register {
  static async CreateUser(params:User) {
    
    //Check if user exists
    let user = users.find(x => x.username === params.username);
    if (user) return false; 
    
    else {
      //Instantiate a newUser and hash the password
      let pass = await bcrypt.hash(params.password, 8);
      let newUser: User = new User (params.username, pass)
      //Append new user to users and serialize to JSON
      users.push(newUser)
      let data = JSON.stringify(users);
      fs.writeFile('users.json', data, (err) => {
        if (err) {
          console.log(err);
        }
      })
      return true;
    }
  }
}

